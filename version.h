/* Date part of package version string. */
#define PACKAGE_DATE_VERSION "20191008"

/* Dotted part of package version string. */
#define PACKAGE_DOTTED_VERSION "1.10.0"

/* Current git commit. */
#define PACKAGE_GIT_COMMIT "1919a4554f4303b5c6398555628bb4aff1fc39d0"
